﻿using System;
using System.IO;

namespace FindAndReplace
{
    class Program
    {
        static void Main(string[] args)
        {
            /*string filePath = @"C:\Users\Matthew George\Documents\Workspace\GroupExercies\week4-pairexercises-team2\file-io-part2-exercises-pair\alices_adventures_in_wonderland.txt";
            string stringSearch = "Alice";
            string replaceString = "Matt";
            string filePathReplaced = @"C:\Users\Matthew George\Documents\Workspace\GroupExercies\week4-pairexercises-team2\file-io-part2-exercises-pair\alices_adventures_in_wonderland_REPLACED.txt";*/

            string filePath = "";
            Console.Write("Enter a file path to search in: ");
            filePath = Console.ReadLine();
            while (!File.Exists(filePath))
            {
                Console.Write("Invalid path. Enter a file path to search in: ");
                filePath = Console.ReadLine();
            }

            string stringSearch = "";
            Console.Write("Enter a search string: ");
            stringSearch = Console.ReadLine();
            while (stringSearch == "")
            {
                Console.Write("Can't enter blank search. Enter a search string: ");
                stringSearch = Console.ReadLine();
            }

            string replaceString = "";
            Console.Write("Enter a replace string: ");
            replaceString = Console.ReadLine();

            string filePathReplaced = "";
            Console.Write("Enter a destination file path with file name and extension: ");
            filePathReplaced = Console.ReadLine();
            while (File.Exists(filePathReplaced))
            {
                Console.Write("File already exists. Enter a destination file path with file name and extension: ");
                filePathReplaced = Console.ReadLine();
            }

            if (File.Exists(filePath))
            {
                using (StreamReader sr = new StreamReader(filePath))
                {
                    try
                    {
                        using (StreamWriter sw = new StreamWriter(filePathReplaced))
                        {
                            while (!sr.EndOfStream)
                            {
                                string line = sr.ReadLine();
                                if (line.Contains(stringSearch))
                                {
                                    line = line.Replace(stringSearch, replaceString);
                                }

                                sw.WriteLine(line);
                            }
                        }
                    }
                    catch(IOException e)
                    {
                        Console.WriteLine("There was an error reading from the file.");
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
    }
}
