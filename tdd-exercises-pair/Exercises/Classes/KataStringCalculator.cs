﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Exercises
{
    public class KataStringCalculator
    {
        public int Add(string numbers)
        {
            int sum = 0;
            List<string> arrayOfSkipChars = new List<string>() { ",", "\n" };
            string input = numbers;
            if (numbers.Length >= 3 && "//" == numbers.Substring(0, 2))
            {
                int magicNumber = numbers.IndexOf('\n');
                string delimiter = numbers.Substring(2, magicNumber - 2);
                arrayOfSkipChars.Add(delimiter);
                input = numbers.Substring(magicNumber + 1);
            }


            if (input.Length > 0)
            {
                string[] working = input.Split(arrayOfSkipChars.ToArray(), StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < working.Length; i++)
                {
                    sum += int.Parse(working[i]);
                }

            }
            return sum;
        }
    }
}
