﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Exercises
{
    public class NumbersToWords
    {
        List<Tuple<int, string>> numberLookup = new List<Tuple<int, string>>()
        {
            new Tuple<int, string>(0, "zero"),
            new Tuple<int, string>(1, "one"),
            new Tuple<int, string>(2, "two"),
            new Tuple<int, string>(3, "three"),
            new Tuple<int, string>(4, "four"),
            new Tuple<int, string>(5, "five"),
            new Tuple<int, string>(6, "six"),
            new Tuple<int, string>(7, "seven"),
            new Tuple<int, string>(8, "eight"),
            new Tuple<int, string>(9, "nine"),
            new Tuple<int, string>(10, "ten"),
            new Tuple<int, string>(11, "eleven"),
            new Tuple<int, string>(12, "twelve"),
            new Tuple<int, string>(13, "thirteen"),
            new Tuple<int, string>(14, "fourteen"),
            new Tuple<int, string>(15, "fifthteen"),
            new Tuple<int, string>(16, "sixteen"),
            new Tuple<int, string>(17, "seventeen"),
            new Tuple<int, string>(18, "eighteen"),
            new Tuple<int, string>(19, "nineteen"),
            new Tuple<int, string>(20, "twenty"),
            new Tuple<int, string>(30, "thirty"),
            new Tuple<int, string>(40, "fourty"),
            new Tuple<int, string>(50, "fifty"),
            new Tuple<int, string>(60, "sixty"),
            new Tuple<int, string>(70, "seventy"),
            new Tuple<int, string>(80, "eighty"),
            new Tuple<int, string>(90, "ninety"),
        };

        private string LookUp(int n)
        {
            foreach(Tuple<int, string> tuple in numberLookup)
            {
                if(n == tuple.Item1)
                {
                    return tuple.Item2;
                }
            }

            return "";
        }
        private int LookUp(string n)
        {
            foreach (Tuple<int, string> tuple in numberLookup)
            {
                if (n == tuple.Item2)
                {
                    return tuple.Item1;
                }
            }

            return -1;
        }

        public int ConvertHundred(List<string> n)
        {
            int num = 0;
            
            foreach (string word in n)
            {
                if (word == "hundred")
                {
                    num *= 100;
                }
                else if (word == "thousand")
                {
                    num *= 1000;
                }
                else
                {
                    num += LookUp(word);
                }
            }

            return num;
        }

        public string Convert(int n)
        {
            string number = "";

            if (n < 20)
            {
                number += LookUp(n);
            }
            else if(n < 100)
            {
                number += LookUp((n / 10) * 10);
                if(n % 10 != 0)
                {
                    number += "-" + Convert(n % 10);
                }
            }
            else if(n < 1000)
            {
                number += LookUp((n / 100));
                number += " hundred";
                if (n % 100 != 0)
                {
                    number += " and " + Convert(n - (n / 100) * 100);
                }
            }
            else if (n < 20000)
            {
                number += LookUp((n / 1000));
                number += " thousand";
                if (n % 1000 != 0)
                {
                    number += " and " + Convert(n - (n / 1000) * 1000);
                }
            }
            else if (n < 100000)
            {
                number += LookUp((n / 10000)* 10);

                if (n % 10000 != 0)
                {
                    number += "-" + Convert(n - (n / 10000) * 10000);
                }
                else if(n % 1000 != 0)
                {
                    number += " thousand and";
                }
                else
                {
                    number += " thousand";
                }
            }
            else if(n < 1000000)
            {
                number += LookUp(n / 100000);
                number += " hundred";
                if(n % 100000 != 0)
                {
                    number += " and " + Convert(n - (n / 100000) * 100000);
                }
                else if(n % 100000 != 0)
                {
                    number += " thousand and";
                }
                else
                {
                    number += " thousand";
                }
            }


            return number;
        }
        public int Convert(string n)
        {
            string[] splitWords = new string[]{ " ", "-"};
            List<string> words = n.Split(splitWords, StringSplitOptions.RemoveEmptyEntries).ToList();
            List<string> hundredWords = new List<string>();
            List<string> thousandWords = new List<string>();
            bool onThousand = words.Contains("thousand");
            for(int i = 0; i < words.Count; i++)
            {
                if(words[i] == "and")
                {
                    continue;
                }
                if (words[i] == "thousand")
                {
                    onThousand = false;
                    continue;
                }

                if(!onThousand)
                {
                    hundredWords.Add(words[i]);
                }
                else
                {
                    thousandWords.Add(words[i]);
                }
            }

            int hundredInt = ConvertHundred(hundredWords);
            int thousandInt = 0;
            if (thousandWords.Count > 0)
            {
                thousandInt = ConvertHundred(thousandWords);
                thousandInt *= 1000;
            }

            return hundredInt + thousandInt;
        }
    }
}
