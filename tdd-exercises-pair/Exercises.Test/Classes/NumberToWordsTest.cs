﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exercises
{
    [TestClass]
    public class NumberToWordsTest
    {
        NumbersToWords testObj = new NumbersToWords();

        #region Numbers to Words
        //1 digit
        [TestMethod]
        public void OneDigit()
        {
            Assert.AreEqual("zero", testObj.Convert(0), "Expected value in string");
            Assert.AreEqual("three", testObj.Convert(3), "Expected value in string");
            Assert.AreEqual("seven", testObj.Convert(7), "Expected value in string");
        }
        [TestMethod]
        public void TwoDigits()
        {
            Assert.AreEqual("ten", testObj.Convert(10), "Expected value in string");
            Assert.AreEqual("fourteen", testObj.Convert(14), "Expected value in string");
            Assert.AreEqual("twenty-six", testObj.Convert(26), "Expected value in string");
        }
        [TestMethod]
        public void ThreeDigits()
        {
            Assert.AreEqual("one hundred and fifty-two", testObj.Convert(152), "You need to account for the hundreds place");
            Assert.AreEqual("two hundred and nine", testObj.Convert(209), "You need to account for the hundreds place");
            Assert.AreEqual("three hundred", testObj.Convert(300), "You need to account for the hundreds place");
            Assert.AreEqual("four hundred and ninety-eight", testObj.Convert(498), "You need to account for the hundreds place");
        }
        [TestMethod]
        public void FourDigits()
        {
            Assert.AreEqual("nine thousand and eight hundred and sixty-three", testObj.Convert(9863), "Account for the thousands place");
            Assert.AreEqual("three thousand and four", testObj.Convert(3004), "Account for the thousands place");
            Assert.AreEqual("five thousand and twenty-six", testObj.Convert(5026), "Account for the thousands place");
            Assert.AreEqual("seven thousand and one hundred and eleven", testObj.Convert(7111), "Account for the thousands place");
        }
        [TestMethod]
        public void FiveDigit()
        {
            Assert.AreEqual("seventy-four thousand and five hundred and eighty-six", testObj.Convert(74586), "Account for the ten thousand's place!!!");
            Assert.AreEqual("fourty thousand", testObj.Convert(40000), "Account for the ten thousand's place!!!");
            Assert.AreEqual("eighty-seven thousand and six hundred and fifty-four", testObj.Convert(87654), "Account for the ten thousand's place!!!");

        }
        [TestMethod]
        public void SixDigit()
        {
            Assert.AreEqual("seven hundred and ninety-eight thousand and nine hundred and eleven", testObj.Convert(798911));
            Assert.AreEqual("five hundred thousand", testObj.Convert(500000));
            Assert.AreEqual("eight hundred and three thousand and three hundred and eight", testObj.Convert(803308));
             Assert.AreEqual("nine hundred and ninety-nine thousand and nine hundred and ninety-nine", testObj.Convert(999999));
        }
        #endregion

        #region Words to Numbers
        [TestMethod]
        public void OneDigitWord()
        {
            Assert.AreEqual(0, testObj.Convert("zero"), "Expected value in string");
            Assert.AreEqual(3, testObj.Convert("three"), "Expected value in string");
            Assert.AreEqual(7, testObj.Convert("seven"), "Expected value in string");
        }
        [TestMethod]
        public void TwoDigitsWord()
        {
            Assert.AreEqual(10, testObj.Convert("ten"), "Expected value in string");
            Assert.AreEqual(14, testObj.Convert("fourteen"), "Expected value in string");
            Assert.AreEqual(26, testObj.Convert("twenty-six"), "Expected value in string");
        }
        [TestMethod]
        public void ThreeDigitsWord()
        {
            Assert.AreEqual(152, testObj.Convert("one hundred and fifty-two"), "You need to account for the hundreds place");
            Assert.AreEqual(209, testObj.Convert("two hundred and nine"), "You need to account for the hundreds place");
            Assert.AreEqual(300, testObj.Convert("three hundred"), "You need to account for the hundreds place");
            Assert.AreEqual(498, testObj.Convert("four hundred and ninety-eight"), "You need to account for the hundreds place");
        }
        [TestMethod]
        public void FourDigitsWord()
        {
            Assert.AreEqual(9863, testObj.Convert("nine thousand and eight hundred and sixty-three"), "Account for the thousands place");
            Assert.AreEqual(3004, testObj.Convert("three thousand and four"), "Account for the thousands place");
            Assert.AreEqual(5026, testObj.Convert("five thousand and twenty-six"), "Account for the thousands place");
            Assert.AreEqual(7111, testObj.Convert("seven thousand and one hundred and eleven"), "Account for the thousands place");
        }
        [TestMethod]
        public void FiveDigitWord()
        {
            Assert.AreEqual(74586, testObj.Convert("seventy-four thousand and five hundred and eighty-six"), "Account for the ten thousand's place!!!");
            Assert.AreEqual(40000, testObj.Convert("fourty thousand"), "Account for the ten thousand's place!!!");
            Assert.AreEqual(87654, testObj.Convert("eighty-seven thousand and six hundred and fifty-four"), "Account for the ten thousand's place!!!");

        }
        [TestMethod]
        public void SixDigitWord()
        {
            Assert.AreEqual(798911, testObj.Convert("seven hundred and ninety-eight thousand and nine hundred and eleven"));
            Assert.AreEqual(500000, testObj.Convert("five hundred thousand"));
            Assert.AreEqual(803308, testObj.Convert("eight hundred and three thousand and three hundred and eight"));
            Assert.AreEqual(999999, testObj.Convert("nine hundred and ninety-nine thousand and nine hundred and ninety-nine"));
        }
        #endregion
    }
}
