﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exercises
{
    [TestClass]
    public class KataStringCalculatorTest
    {
        KataStringCalculator test;

        private void CreateBlankSlate()
        {
            test = new KataStringCalculator();
        }
        //Tests

        //Test if 0 numbers
        [TestMethod]
        public void EmptyString()
        {
            CreateBlankSlate();
            Assert.AreEqual(0, test.Add(""), "Empty string expected");
        }
        
        //Test if 1 number
        [TestMethod]
        public void OneNumber()
        {
            CreateBlankSlate();
            Assert.AreEqual(4, test.Add("4"), "Expected result is number passed");
        }

        //test if 2 numbers
        [TestMethod]
        public void TwoNumbers()
        {
            CreateBlankSlate();
            Assert.AreEqual(5, test.Add("2,3"), "Expected result is sum of two numbers");
        }

        [TestMethod]
        public void UnknownLengthNumbers()
        {
            CreateBlankSlate();
            Assert.AreEqual(13, test.Add("1,5,7"), "Result expected was 13.");
            Assert.AreEqual(25, test.Add("1,10,11,3"), "Result expected was 25.");
        }

        [TestMethod]
        public void HandleNewline()
        {
            CreateBlankSlate();
            Assert.AreEqual(6, test.Add("1\n2,3"), "Result expected was 6.");
            Assert.AreEqual(14, test.Add("3\n5\n2,4"), "Result expected was 14.");
        }
        [TestMethod]
        public void DelimiterTest()
        {
            CreateBlankSlate();
            Assert.AreEqual(3, test.Add("//;\n1;2"), "Result expected is 3");
            Assert.AreEqual(13, test.Add("//!\n4!9"), "Result expected is 13");
        }
    }
}
