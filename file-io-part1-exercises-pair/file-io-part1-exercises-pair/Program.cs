﻿using System;
using System.IO;
using System.Collections.Generic;

namespace file_io_part1_exercises_pair
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePath = "";
            do
            {
                Console.Write("Please enter in the file path: ");
                filePath = Console.ReadLine();
            }
            while (!File.Exists(filePath));

            //manually putting in the file path
            //filePath = @"C:\Users\Corey Jansen\workspace\week4-pairexercises-team2\file-io-part1-exercises-pair";
            //filePath += @"\alices_adventures_in_wonderland.txt";

            int wordCount = 0;
            int sentenceCount = 0;

            using (StreamReader er = new StreamReader(filePath))
            {
                List<string> endOfSentence = new List<string>();
                string[] thingsToRemove = { "_", " ", "\n", "·", "\t", "*", ")", "\"", "--" };
                List<string> notSentences = new List<string>() { "I.", "II.", "III.", "IV.", "V.", "VI.", "VII.", "VIII.", "IX.", "X.", "XI.", "XII.", "Dr.", "Esq.", "W.", "Mr.", "Mrs.", "Ms.", "1.", "1.A.",
                        "1.E.8.", "1.C.", "1.D.", "1.E.", "1.E.1.", "1.E.9.", "1.E.3.", "1.E.4.", "1.E.5.", "1.E.6.", "1.E.7.", "U.S.", "5.", "S.", "B.", "4.", "3.", "2.", "1.F.6.", "1.F.5.",
                    "1.F.4.", "1.F.3.", "F3.", "1.F.2.", "1.F.1.", "1.F.", "1.E.2.", "1.B."};
                while (!er.EndOfStream)
                {
                    string line = er.ReadLine();
                    string[] splitString = line.Split(thingsToRemove, StringSplitOptions.RemoveEmptyEntries);

                    //assumption coming into this for loop is that every entry in splitString is a word
                    //sentences will be noted by words that end in a '.' '?' or '!'
                    for (int i = 0; i < splitString.Length; i++)
                    {
                        //add int later: functionality to catch words broken across different lines
                        wordCount++;

                        int lastIndex = splitString[i].Length - 1;
                        char lastChar = splitString[i].ToString()[lastIndex];
                        if ((lastChar == '.' || lastChar == '?' || lastChar == '!') && !notSentences.Contains(splitString[i]))
                        {
                            sentenceCount++;
                            endOfSentence.Add(splitString[i]);
                        }
                    }
                }
            }


            Console.WriteLine("Here is your output:");
            Console.WriteLine($"Alice in wonderland has {wordCount} words.");
            Console.WriteLine($"It also has {sentenceCount} sentences.");
            Console.ReadLine();
        }
    }
}
